#pragma region enum
enum portEnum {
	port1 = 1,
	port2 = 2,
	port3 = 3,
	port4 = 4,
	port5 = 5,
	port6 = 6,
	port7 = 7,
	port8 = 8,
	port9 = 9,
	port10 = 10,
	port11 = 11,
	port12 = 12,
	port13 = 13
};
enum analogSigal {
	analogLow = 0,
	analogHigh = 255
};

enum streeingValue {
	defaultValue = 128,
	left = 64,
	right = 172
};
#pragma endregion

#pragma region interrupt
class interrupt {
private:
	unsigned long startTimer;
	int interval;

public:
	void setInterrupt(int setInterval) {
		startTimer = millis();
		interval = setInterval;
	}

	interrupt(int setInterval) {
		startTimer = millis();
		interval = setInterval;
	};

	boolean shouldRun()
	{
		unsigned long currentTime = millis();
		if (currentTime >= startTimer + interval)
		{
			startTimer = currentTime;
			return true;
		}
		return false;
	}
};
#pragma endregion

#pragma region motor controll
class WheelController
{
public:
	void run()
	{
		if (!isRunning())
		{
			analogWrite(port, 255);
			running = true;
		}
	}
	void stop()
	{
		if (isRunning())
		{
			analogWrite(port, 0);
			running = false;
		}
	}

	boolean isRunning()
	{
		return running;
	}

	WheelController() {};
	WheelController(uint8_t wheelPort)
	{
		port = wheelPort;
	}

private:
	uint8_t port;
	boolean running = false;
};

class MotorsMasterController
{
private:
	WheelController mainWheel = WheelController();

public:
	MotorsMasterController() {};

	MotorsMasterController(uint8_t enginPort, uint8_t steeringPort)
	{
		mainWheel = WheelController(enginPort);
	};

	void start()
	{
		if (!mainWheel.isRunning())
		{
			mainWheel.run();
		}
	}

	void stop() {
		if (mainWheel.isRunning())
		{
			mainWheel.stop();
		}
	}

	void turnLeft()
	{

	}

	void turnRight()
	{

	}

	boolean isRunning()
	{
		return mainWheel.isRunning();
	}
};
#pragma endregion

#pragma region streeing
class StreeingController
{
private:
	uint8_t port;
	int turnningValue = streeingValue::defaultValue;
	void Turn()
	{
		analogWrite(port, turnningValue);
	}

public:
	StreeingController() {	};
	StreeingController(uint8_t streeingPort)
	{
		port = streeingPort;
		Reset();
	};

	void Reset()
	{
		turnningValue = streeingValue::defaultValue;
		Turn();
	}

	void TurnLeft()
	{
		turnningValue = streeingValue::left;
		Turn();
	}

	void TurRight()
	{
		turnningValue = streeingValue::right;
		Turn();
	}
};
#pragma endregion


#pragma region read Sensor
class readSensor : private interrupt
{
private:
	uint8_t display;
	int lastTimeValue;

public:
	readSensor(uint8_t port, int interval) : interrupt(interval) {
		read();
	};

	readSensor() :interrupt(NULL) {};

	int read()
	{
		lastTimeValue = analogRead(display);
		return lastTimeValue;
	};

	int readWithInterval()
	{
		if (shouldRun())
		{
			lastTimeValue = analogRead(display);
		}
		return lastTimeValue;
	};
};
#pragma endregion

#pragma region LED Controller
class LEDDisplay : public interrupt
{
public:
	LEDDisplay(int interrupInterval, uint8_t displayPort) :interrupt(interrupInterval)
	{
		port = displayPort;
	}

	void analogOn(int value)
	{
		analogWrite(port, value);
		if (value != 0)
		{
			analogIsOn = true;
		}
		else
		{
			analogIsOn = false;
		}
	}

	void analogClear()
	{
		analogWrite(port, 0);
		analogIsOn = false;
	}

	void digitalOn()
	{
		if (!isDigitalOn())
		{
			digitalWrite(port, HIGH);
			digitalIsOn = true;
		}
	}

	void digitalOff()
	{
		if (isDigitalOn())
		{
			digitalWrite(port, LOW);
			digitalIsOn = false;
		}
	}

	void digitalToggle()
	{
		if (isDigitalOn())
		{
			digitalOff();
		}
		else
		{
			digitalOn();
		}
	}

	boolean isDigitalOn()
	{
		return digitalIsOn;
	}

	boolean isAnalofOn()
	{
		return analogIsOn;
	}
private:
	uint8_t port;
	boolean digitalIsOn = false;
	boolean analogIsOn = false;
};

class LEDAnalogController : private LEDDisplay
{
private:
	int readSensorCounter = 0;
	boolean readSensorIncreasing = true;

public:
	LEDAnalogController(int interrupInterval, uint8_t displayPort) :LEDDisplay(interrupInterval, displayPort) {	};

	void repeatOnAndOff()
	{
		if (shouldRun())
		{
			if (readSensorCounter - 2 <= 0)
			{
				readSensorIncreasing = true;
			}
			if (readSensorCounter < 255 && readSensorIncreasing)
			{
				readSensorCounter++;
			}
			else if (readSensorCounter >= 255)
			{
				readSensorIncreasing = false;
				readSensorCounter -= 2;
			}
			else if (!readSensorIncreasing && readSensorCounter - 2 >= 0)
			{
				readSensorCounter -= 2;
			}
			//Serial.print(port);
			//Serial.print(": ");
			//Serial.print(readSensorCounter);
			//Serial.print("\n");
			analogOn(readSensorCounter);
		}
	}

	void on(int level)
	{
		if (shouldRun())
		{
			analogOn(level);
		}
	}

	void toggle(int level)
	{
		if (shouldRun())
		{
			if (isAnalofOn())
			{
				analogClear();
			}
			else
			{
				analogOn(level);
			}
		}
	}
};

class LEDDigitalController : private LEDDisplay
{
public:
	LEDDigitalController(int interrupInterval, uint8_t displayPort) : LEDDisplay(interrupInterval, displayPort) {};

	void on()
	{
		if (shouldRun())
		{
			Serial.print("On LED");
			digitalOn();
		}
	}

	void off()
	{
		if (shouldRun())
		{
			digitalOff();
		}
	}

	void toggle() {
		if (shouldRun())
		{
			Serial.print("toggle");
			digitalToggle();
		}
	}
};
#pragma endregion

#pragma region running controll
class runningControll : private interrupt {
public:
	MotorsMasterController motorsMasterController;
	StreeingController streeingController;
	readSensor* sensor;
	int sensorSize;
private:
	runningControll(uint8_t enginPort, uint8_t steeringPort, int sensorPorts[], int sizeOfSensorPort, int interval) :interrupt(interval) {
		motorsMasterController = MotorsMasterController(enginPort, steeringPort);

		sensorSize = sizeOfSensorPort;
		for (int counter = 0; counter < sensorSize; counter++)
		{
			sensor[counter] = readSensor(sensorPorts[counter], interval);
		}
	};

	void start() {
		motorsMasterController.start();
	};

	void autoStart() {
		if (shouldRun())
		{
			//int reading = readSensor
		}
	};

	void stop() {

	};
};
#pragma endregion

void setup()
{
	/* add setup code here */
	Serial.begin(9600);
}

//display LED, port2-5
//pohotdiao, portA0-A3
//motor, port 9
//sterr, port 11

const int wheelPort = port10;
const int steeringPort = port1;
LEDAnalogController frontLED = LEDAnalogController(100, port2);
LEDAnalogController stopLED = LEDAnalogController(200, port3);
LEDAnalogController turnLeftLED = LEDAnalogController(300, port4);
LEDAnalogController turnRightLED = LEDAnalogController(400, port5);
MotorsMasterController motorObject = MotorsMasterController(wheelPort, steeringPort);
interrupt test = interrupt(1000);
void loop()
{
	stopLED.toggle(analogHigh);
	frontLED.toggle(analogHigh);
	turnLeftLED.toggle(analogHigh);
	turnRightLED.toggle(analogHigh);
	//if (test.shouldRun())
	//{
	//	if (digitalRead(0) == 1)
	//	{
	//		//Serial.println("Start the wheel");
	//		motorObject.start();
	//	}
	//	else
	//	{
	//		//Serial.println("Stop Stop Stop the wheel");
	//		motorObject.stop();
	//	}
	//}
}
